package sps.enlighten.groundsystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import org.junit.Assert;

import com.google.gson.Gson;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import sps.enlighten.groundsystem.dto.OutputDTO;


/**
 * Unit test for the Enlighten Ground System application.
 */
public class EnlightenGroundSystemTest extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public EnlightenGroundSystemTest(String testName)
    {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite(EnlightenGroundSystemTest.class);
    }

    /**
     * Run all the tests.
     */
    public void testApp()
    {
    	// Test with the EITC sample data.
        sampleDataTest();
        
        // Test a special case.
        duplicateMessageAndCrLfTest();
        
        // Additional test cases.
        // The previous two test could have been put into this method too, 
        //    but were intentionally singled out.
        genericTestExecutor();
    }

    /**
     * Test using the sample data provided by Enlightened IT Consulting.
     */
    public void sampleDataTest() {
        
        System.out.println("INFO: Runing Unit Test using EITC-Supplied sample data...");
        
        // Make sure the telemetry archive file processor is starting with a clean slate before running this test.  
        TelemetryHandler.reset();
        
        // Sample data from Enlightened Programming Challenge web site.
        String sampleTestData="20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT\n"+
                              "20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT\n"+
                              "20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT\n"+
                              "20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT\n"+
                              "20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT\n"+
                              "20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT\n"+
                              "20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT\n"+
                              "20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT\n"+
                              "20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT\n"+
                              "20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT\n"+
                              "20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT\n"+
                              "20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT\n"+
                              "20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT\n"+
                              "20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT";
        
        // Define the expected results.
        OutputDTO[] expectedResults=new OutputDTO[2];
        expectedResults[0]=new OutputDTO(1000, "RED HIGH", "TSTAT", "2018-01-01T23:01:38.001Z");
        expectedResults[1]=new OutputDTO(1000, "RED LOW", "BATT", "2018-01-01T23:01:09.521Z");
        
        // Create a temporary file, to be deleted on test when all the JUNIT test module ends.
        File f=null;
        try {
            f=File.createTempFile("abc", "xyz");
            f.deleteOnExit();
            
        } catch (SecurityException E) {
            Assert.fail("Temporary file location is write-protected <"+E.getLocalizedMessage()+">");
        } catch (IOException E) {
            Assert.fail("Unable to create a temporary file for testing <"+E.getLocalizedMessage()+">");
        } catch (Exception E) {
            Assert.fail("Unexpected Exception <"+E.getLocalizedMessage()+">");
        }

        // Write the test data to the temporary file and then process the created file.
        try (PrintWriter out=new PrintWriter(f.getAbsolutePath())) {
            
            // Write the test data to the temporary file.
            out.println(sampleTestData);
            out.close();
            
            // Send the temporary file to the process telemetry file method.
            EnlightenGroundSystem.processTelemetryFile(f.getAbsolutePath());
            
            // Retrieve all the notifications resulting from the processed telemetry archive file.
            String results=TelemetryHandler.createResultJson();
            
            // Check the notification message count.
            Gson gson=new Gson();
            OutputDTO[] notifications=gson.fromJson(results, OutputDTO[].class);
            Assert.assertEquals(2, notifications.length);

            // Verify the calculated results match the expected results.
            boolean allFound=true;
            for (OutputDTO notification:notifications) {
                boolean recordFound=false;
                for (OutputDTO expectedResult:expectedResults) {
                    if (expectedResult.getTimestamp().equalsIgnoreCase(notification.getTimestamp()) &&
                            expectedResult.getSatelliteId().intValue()==notification.getSatelliteId().intValue() &&
                            expectedResult.getComponent().equalsIgnoreCase(notification.getComponent())) {
                        recordFound=true;
                        Assert.assertEquals(expectedResult.getSatelliteId().intValue(), 
                                notification.getSatelliteId().intValue());
                        Assert.assertEquals(expectedResult.getSeverity(), notification.getSeverity());
                        Assert.assertEquals(expectedResult.getComponent(), notification.getComponent());
                    }
                }
                allFound&=recordFound;
            } 
            Assert.assertTrue(allFound);
            
        } catch (SecurityException E) {
            Assert.fail("Temporary file location is write-protected <"+E.getLocalizedMessage()+">");
        } catch (FileNotFoundException E) {
            Assert.fail("Unable to write to the temporary file for testing <"+E.getLocalizedMessage()+">");
        } catch (Exception E) {
            E.printStackTrace();
            Assert.fail("Unexpected Exception <"+E.getLocalizedMessage()+">");
        }
        System.out.println("INFO: Completed.");
    }

    
    /**
     * Testing for duplicate messages and the use of CR-LF  (\r\n
     */
    public void duplicateMessageAndCrLfTest() {
        
        System.out.println("INFO: Runing Unit Test to check for de-duplication of operator notifications...");
        
        // Make sure the telemetry archive file processor is starting with a clean slate before running this test.  
        TelemetryHandler.reset();
        
        // Data containing 4 failures in a row.
        String sampleTestData="20191021 00:00:00.000|1000|101|98|25|20|95|TSTAT\r\n"+
                              "20191021 00:00:00.000|1001|17|15|9|8|12|BATT\r\n"+
                              "20191021 00:01:00.000|1000|101|98|25|20|95|TSTAT\r\n"+
                              "20191021 00:01:00.000|1001|17|15|9|8|12|BATT\r\n"+
                              "20191021 00:02:00.000|1000|101|98|25|20|95|TSTAT\r\n"+
                              "20191021 00:02:00.000|1001|17|15|9|8|12|BATT\r\n"+
                              "20191021 00:03:00.000|1000|101|98|25|20|195|TSTAT\r\n"+
                              "20191021 00:03:00.000|1001|17|15|9|8|2|BATT\r\n"+
                              "20191021 00:04:00.000|1000|101|98|25|20|195|TSTAT\r\n"+
                              "20191021 00:04:00.000|1001|17|15|9|8|2|BATT\r\n"+
                              "20191021 00:05:00.000|1000|101|98|25|20|195|TSTAT\r\n"+
                              "20191021 00:05:00.000|1001|17|15|9|8|2|BATT\r\n"+
                              "20191021 00:06:00.000|1000|101|98|25|20|195|TSTAT\r\n"+
                              "20191021 00:06:00.000|1001|17|15|9|8|2|BATT\r\n"+
                              "20191021 00:07:00.000|1000|101|98|25|20|95|TSTAT\r\n"+
                              "20191021 00:07:00.000|1001|17|15|9|8|12|BATT\r\n"+
                              "20191021 00:08:00.000|1000|101|98|25|20|95|TSTAT\r\n"+
                              "20191021 00:08:00.000|1001|17|15|9|8|12|BATT\r\n"+
                              "20191021 00:09:00.000|1000|101|98|25|20|95|TSTAT\r\n"+
                              "20191021 00:09:00.000|1001|17|15|9|8|12|BATT\r\n";
                              
        // Define the expected results.
        OutputDTO[] expectedResults=new OutputDTO[4];
        expectedResults[0]=new OutputDTO(1000, "RED HIGH", "TSTAT", "2019-10-21T00:03:00.000Z");
        expectedResults[1]=new OutputDTO(1001, "RED LOW",  "BATT",  "2019-10-21T00:03:00.000Z");
        expectedResults[2]=new OutputDTO(1000, "RED HIGH", "TSTAT", "2019-10-21T00:04:00.000Z");
        expectedResults[3]=new OutputDTO(1001, "RED LOW",  "BATT",  "2019-10-21T00:04:00.000Z");
        
        // Create a temporary file, to be deleted on test when all the JUNIT test module ends.
        File f=null;
        try {
            f=File.createTempFile("abc", "xyz");
            f.deleteOnExit();
            
        } catch (SecurityException E) {
            Assert.fail("Temporary file location is write-protected <"+E.getLocalizedMessage()+">");
        } catch (IOException E) {
            Assert.fail("Unable to create a temporary file for testing <"+E.getLocalizedMessage()+">");
        } catch (Exception E) {
            Assert.fail("Unexpected Exception <"+E.getLocalizedMessage()+">");
        }

        // Write the test data to the temporary file and then process the created file.
        try (PrintWriter out=new PrintWriter(f.getAbsolutePath())) {
            
            // Write the test data to the temporary file.
            out.println(sampleTestData);
            out.close();
            
            // Send the temporary file to the process telemetry file method.
            EnlightenGroundSystem.processTelemetryFile(f.getAbsolutePath());
            
            // Retrieve all the notifications resulting from the processed telemetry archjive file.
            String results=TelemetryHandler.createResultJson();
            
            // Check the notification message count.
            Gson gson=new Gson();
            OutputDTO[] notifications=gson.fromJson(results, OutputDTO[].class);
            Assert.assertEquals(4, notifications.length);

            // Verify the calculated results match the expected results.
            boolean allFound=true;
            for (OutputDTO notification:notifications) {
                boolean recordFound=false;
                for (OutputDTO expectedResult:expectedResults) {

                    if (expectedResult.getTimestamp().equalsIgnoreCase(notification.getTimestamp()) &&
                            expectedResult.getSatelliteId().intValue()==notification.getSatelliteId().intValue() &&
                            expectedResult.getComponent().equalsIgnoreCase(notification.getComponent())) {
                        recordFound=true;
                        Assert.assertEquals(expectedResult.getSatelliteId().intValue(), 
                                notification.getSatelliteId().intValue());
                        Assert.assertEquals(expectedResult.getSeverity(), notification.getSeverity());
                        Assert.assertEquals(expectedResult.getComponent(), notification.getComponent());
                    }
                }
                allFound&=recordFound;
            } 
            Assert.assertTrue(allFound);
            
        } catch (SecurityException E) {
            Assert.fail("Temporary file location is write-protected <"+E.getLocalizedMessage()+">");
        } catch (FileNotFoundException E) {
            Assert.fail("Unable to write to the temporary file for testing <"+E.getLocalizedMessage()+">");
        } catch (Exception E) {
            E.printStackTrace();
            Assert.fail("Unexpected Exception <"+E.getLocalizedMessage()+">");
        }
        System.out.println("INFO: Completed.");
    }


    /**
     * Generic test executor -- Loops through the test data lookup and runs all suppied input/output data combinations.  
     */
    public void genericTestExecutor() {
        System.out.println("INFO: Running Supplemental Unit Tests...");
        
        boolean done=false;
        int testCounter=1;
        while (!done) {

            // Make sure the telemetry archive file processor is starting with a clean slate before running this test.  
            TelemetryHandler.reset();
            
            HashMap<String,String> test=getTestData(testCounter);
            
            String sampleTestData=test.get("Input");
            
            OutputDTO[] expectedResults=null;
            
            if (sampleTestData.equalsIgnoreCase("Last Test")) done=true;
            else {
                System.out.println("INFO: Running Supplemental Unit Test number "+testCounter+"...");

                String outputDataAll=test.get("Output");
                
                // NULL means no notifications to send
                if (outputDataAll!=null) {
                    String[] outputData=outputDataAll.split("\\n");
                    
                    // Define the expected results.
                    expectedResults=new OutputDTO[outputData.length];
                    for (int i=0; i<outputData.length; i++) 
                        expectedResults[i]=new OutputDTO(outputData[i]);
                }
            
                // Create a temporary file, to be deleted on test when all the JUNIT test module ends.
                File f=null;
                try {
                    f=File.createTempFile("abc", "xyz");
                    f.deleteOnExit();
                    
                } catch (SecurityException E) {
                    Assert.fail("Temporary file location is write-protected <"+E.getLocalizedMessage()+">");
                } catch (IOException E) {
                    Assert.fail("Unable to create a temporary file for testing <"+E.getLocalizedMessage()+">");
                } catch (Exception E) {
                    Assert.fail("Unexpected Exception <"+E.getLocalizedMessage()+">");
                }
        
                // Write the test input data to the temporary file and then process the created file.
                try (PrintWriter out=new PrintWriter(f.getAbsolutePath())) {
                    
                    // Write the test data to the temporary file.
                    out.println(sampleTestData);
                    out.close();
                    
                    // Send the temporary file to the process telemetry file method.
                    EnlightenGroundSystem.processTelemetryFile(f.getAbsolutePath());
                    
                    // Retrieve all the notifications resulting from the processed telemetry archjive file.
                    String results=TelemetryHandler.createResultJson();
                    
                    // Check the notification message count.
                    Gson gson=new Gson();
                    OutputDTO[] notifications=gson.fromJson(results, OutputDTO[].class);
                    if (expectedResults!=null)
                        Assert.assertEquals(expectedResults.length, notifications.length);
                    else Assert.assertEquals(0, notifications.length);
        
                    // Verify the calculated results match the expected results.
                    boolean allFound=true;
                    for (OutputDTO notification:notifications) {
                        boolean recordFound=false;
                        for (OutputDTO expectedResult:expectedResults) {
        
                            if (expectedResult.getTimestamp().equalsIgnoreCase(notification.getTimestamp()) &&
                                    expectedResult.getSatelliteId().intValue()==notification.getSatelliteId().intValue() &&
                                    expectedResult.getComponent().equalsIgnoreCase(notification.getComponent())) {
                                recordFound=true;
                                Assert.assertEquals(expectedResult.getSatelliteId().intValue(), 
                                        notification.getSatelliteId().intValue());
                                Assert.assertEquals(expectedResult.getSeverity(), notification.getSeverity());
                                Assert.assertEquals(expectedResult.getComponent(), notification.getComponent());
                            }
                        }
                        allFound&=recordFound;
                    } 
                    Assert.assertTrue(allFound);
                    
                } catch (SecurityException E) {
                    Assert.fail("Temporary file location is write-protected <"+E.getLocalizedMessage()+">");
                } catch (FileNotFoundException E) {
                    Assert.fail("Unable to write to the temporary file for testing <"+E.getLocalizedMessage()+">");
                } catch (Exception E) {
                    E.printStackTrace();
                    Assert.fail("Unexpected Exception <"+E.getLocalizedMessage()+">");
                }
                
                System.out.println("INFO: Completed.");
            }
            testCounter++;
        }
        System.out.println("INFO: Supplemental Unit Tests Completed");
    }
    
    HashMap<String,String> getTestData(int testNumber) {
        HashMap<String,String> retval=new HashMap<>();
        
        switch (testNumber) {
        case 1:  // Sanity Check -- similar data used in the PrimaryUnitTests.duplicateMessageAndCrLfTest() test.
            retval.put("Input",
                    "20191021 00:00:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:00:00.000|1001|17|15|9|8|12|BATT\n"+
                    "20191021 00:01:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:01:00.000|1001|17|15|9|8|12|BATT\n"+
                    "20191021 00:02:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:02:00.000|1001|17|15|9|8|12|BATT\n"+
                    "20191021 00:03:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:03:00.000|1001|17|15|9|8|2|BATT\n"+
                    "20191021 00:04:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:04:00.000|1001|17|15|9|8|2|BATT\n"+
                    "20191021 00:05:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:05:00.000|1001|17|15|9|8|2|BATT\n"+
                    "20191021 00:06:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:06:00.000|1001|17|15|9|8|2|BATT\n"+
                    "20191021 00:07:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:07:00.000|1001|17|15|9|8|12|BATT\n"+
                    "20191021 00:08:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:08:00.000|1001|17|15|9|8|12|BATT\n"+
                    "20191021 00:09:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:09:00.000|1001|17|15|9|8|12|BATT\n"+
                    "20191021 00:10:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:10:00.000|1001|17|15|9|8|12|BATT\n"+
                    "20191021 00:11:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:11:00.000|1001|17|15|9|8|12|BATT\n");
            retval.put("Output", 
                    "1000|RED HIGH|TSTAT|2019-10-21T00:03:00.000Z\n"+
                    "1001|RED LOW|BATT|2019-10-21T00:03:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:04:00.000Z\n"+
                    "1001|RED LOW|BATT|2019-10-21T00:04:00.000Z");
            break;
        case 2:
            retval.put("Input",
                    "20191021 00:00:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:01:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:02:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:03:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:04:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:05:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:06:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:07:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:08:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:09:00.000|1000|101|98|25|20|95|TSTAT\n");
            retval.put("Output", 
                    "1000|RED HIGH|TSTAT|2019-10-21T00:00:00.000Z\n");
            break;
        case 3:
            retval.put("Input",
                    "20191021 00:00:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:01:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:02:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:03:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:04:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:05:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:06:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:07:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:08:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:09:00.000|1000|101|98|25|20|95|TSTAT\n");
            retval.put("Output", null);
            break;
        case 4:
            retval.put("Input",
                    "20191021 00:00:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:01:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:02:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:03:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:04:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:05:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:06:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:07:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:08:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:09:00.000|1000|101|98|25|20|195|TSTAT\n");
            retval.put("Output", null);
            break;
        case 5:
            retval.put("Input",
                    "20191021 00:00:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:01:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:02:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:03:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:04:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:05:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:06:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:07:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:08:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:09:00.000|1000|101|98|25|20|195|TSTAT\n");
            retval.put("Output", 
                    "1000|RED HIGH|TSTAT|2019-10-21T00:07:00.000Z\n");
            break;
        case 6:
            retval.put("Input",
                    "20191021 00:00:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:01:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:02:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:03:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:04:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:05:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:06:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:07:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:08:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:09:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:10:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:11:00.000|1000|101|98|25|20|95|TSTAT\n");
            retval.put("Output", null);
            break;
        case 7:
            retval.put("Input",
                    "20191021 00:00:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:01:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:02:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:03:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:04:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:05:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:06:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:07:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:08:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:09:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:10:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:11:00.000|1000|101|98|25|20|95|TSTAT\n");
            retval.put("Output", 
                    "1000|RED HIGH|TSTAT|2019-10-21T00:02:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:04:00.000Z\n");
            break;
        case 8:
            retval.put("Input",
                    "20191021 00:00:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:01:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:02:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:03:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:04:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:05:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:06:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:07:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:08:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:09:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:10:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:11:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:12:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:13:00.000|1000|101|98|25|20|95|TSTAT\n");
            retval.put("Output", 
                    "1000|RED HIGH|TSTAT|2019-10-21T00:02:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:03:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:06:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:07:00.000Z\n");
            break;
        case 9:
            retval.put("Input",
                    "20191021 00:00:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:01:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:02:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:03:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:04:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:05:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:06:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:07:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:08:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:09:00.000|1000|101|98|25|20|95|TSTAT\n");
            retval.put("Output", null);
            break;
        case 10:
            retval.put("Input",
                    "20191021 00:00:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:01:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:02:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:03:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:04:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:05:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:06:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:07:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:08:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:09:00.000|1000|101|98|25|20|195|TSTAT\n");
            retval.put("Output", 
                    "1000|RED HIGH|TSTAT|2019-10-21T00:00:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:01:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:02:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:03:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:04:00.000Z\n");
                // NOTE: A message is not expected for 2019-10-21T00:05:00.000Z or later because the last
                //       input telemetry item is 2019-10-21T00:09:00.000Z.  Subtract 5 minutes and you get
                //       2019-10-21T00:04:00.000Z.   This is considered the last notification time.
                //       If instead the timestamp was 2019-10-21T00:03:59.999Z, then a message would be seen
                //       for 2019-10-21T00:05:00.000Z.  See test case 11.
            break;
        case 11:
            retval.put("Input",
                    "20191021 00:00:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:01:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:02:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:03:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:03:59.999|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:05:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:06:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:07:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:08:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:09:00.000|1000|101|98|25|20|195|TSTAT\n");
            retval.put("Output", 
                    "1000|RED HIGH|TSTAT|2019-10-21T00:00:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:01:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:02:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:03:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:05:00.000Z\n");
                // NOTE: No message is expected for 2019-10-21T00:03:59.999Z as checks on for 3 violations 
                //       within 5 minutes are done based only on the timestamp of the newest telemetry item.
                //       In this case, the check was done based on "20191021 00:08:00.000" and "20191021 00:09:00.000".
                //       For "20191021 00:08:00.000", the first limit violation within the prior 5 minutes was at 03:00.000
                //       For "20191021 00:09:00.000", the first limit violation within the prior 5 minutes was at 04:00.000
            break;
        case 12:
            retval.put("Input",
                    "20191021 00:00:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:01:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:02:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:03:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:03:59.999|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:05:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:06:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:07:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:08:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:09:00.000|1000|101|98|25|20|95|TSTAT\n");
            retval.put("Output", 
                    "1000|RED HIGH|TSTAT|2019-10-21T00:00:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:01:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:02:00.000Z\n"+
                    "1000|RED HIGH|TSTAT|2019-10-21T00:03:00.000Z\n");
                // NOTE: No message is expected for 2019-10-21T00:03:59.999Z for the same reasons as in test case 11.
            break;
        case 13:
            retval.put("Input",
                    "20191021 00:00:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:01:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:02:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:03:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:03:59.996|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:03:59.997|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:03:59.998|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:03:59.999|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:04:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:05:00.000|1000|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:06:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:07:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:08:00.000|1000|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:09:00.000|1000|101|98|25|20|95|TSTAT\n");
            retval.put("Output", 
                    "1000|RED HIGH|TSTAT|2019-10-21T00:03:59.998Z\n");
                // NOTE: No message is expected for 2019-10-21T00:03:59.999Z for the same reasons as in test cases 11 and 12.
            break;
        case 14:  // Testing alternate satellite ID to show the code can handle different sat IDs.
            retval.put("Input",
                    "20191021 00:00:00.000|99|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:01:00.000|99|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:02:00.000|99|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:03:00.000|99|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:04:00.000|99|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:05:00.000|99|101|98|25|20|195|TSTAT\n"+
                    "20191021 00:06:00.000|99|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:07:00.000|99|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:08:00.000|99|101|98|25|20|95|TSTAT\n"+
                    "20191021 00:09:00.000|99|101|98|25|20|95|TSTAT\n");
            retval.put("Output", 
                    "99|RED HIGH|TSTAT|2019-10-21T00:03:00.000Z\n");
            break;
        case 15:
            retval.put("Input",
                    "20191021 00:00:00.000|1001|17|15|9|8|12|BATT\n"+
                    "20191021 00:01:00.000|1001|17|15|9|8|12|BATT\n"+
                    "20191021 00:02:00.000|1001|17|15|9|8|12|BATT\n"+
                    "20191021 00:03:00.000|1001|17|15|9|8|5|BATT\n"+
                    "20191021 00:04:00.000|1001|17|15|9|8|5|BATT\n"+
                    "20191021 00:05:00.000|1001|17|15|9|8|5|BATT\n"+
                    "20191021 00:06:00.000|1001|17|15|9|8|5|BATT\n"+
                    "20191021 00:07:00.000|1001|17|15|9|8|12|BATT\n"+
                    "20191021 00:08:00.000|1001|17|15|9|8|12|BATT\n"+
                    "20191021 00:09:00.000|1001|17|15|9|8|12|BATT\n");
            retval.put("Output", 
                    "1001|RED LOW|BATT|2019-10-21T00:03:00.000Z\n"+
                    "1001|RED LOW|BATT|2019-10-21T00:04:00.000Z\n");
            break;
        case 16:
            retval.put("Input",
                    "20191021 00:00:00.000|11|17|15|9|8|12|BATT\n"+
                    "20191021 00:01:00.000|11|17|15|9|8|12|BATT\n"+
                    "20191021 00:02:00.000|11|17|15|9|8|12|BATT\n"+
                    "20191021 00:03:00.000|11|17|15|9|8|5|BATT\n"+
                    "20191021 00:04:00.000|11|17|15|9|8|5|BATT\n"+
                    "20191021 00:05:00.000|11|17|15|9|8|5|BATT\n"+
                    "20191021 00:06:00.000|11|17|15|9|8|5|BATT\n"+
                    "20191021 00:07:00.000|11|17|15|9|8|12|BATT\n"+
                    "20191021 00:08:00.000|11|17|15|9|8|12|BATT\n"+
                    "20191021 00:09:00.000|11|17|15|9|8|12|BATT\n");
            retval.put("Output", 
                    "11|RED LOW|BATT|2019-10-21T00:03:00.000Z\n"+
                    "11|RED LOW|BATT|2019-10-21T00:04:00.000Z\n");
            break;
        default:
            retval.put("Input", "Last Test");
            retval.put("Output", "Last Test");
        }
        
        return retval;
    }
}
