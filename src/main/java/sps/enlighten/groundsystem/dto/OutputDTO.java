package sps.enlighten.groundsystem.dto;

import lombok.Getter;
import lombok.Setter;
import sps.enlighten.groundsystem.constants.Constants;

/**
 * Class containing the data needed for operator notifications.
 * 
 * @author Shawn Scoles
 *
 */
@Getter
@Setter
public class OutputDTO {

	/**
	 * The satellite ID of the limit violation.
	 */
	Integer satelliteId;
	
	/**
	 * For the purpose of this programming challenge, only "RED HIGH" or "RED LOW" are valid.
	 */
	String severity;
	
	/**
	 * Which component?  For the purpose of this programming challenge, only BATT and TSTAT are valid.
	 *    BATT: Battery Voltage
	 *    TSTAT: Thermostat Temperature
	 */	
	String component;
	
	/**
	 * Looking at the example, this appears to be the time stamp of the first occurrence of the 
	 * three limit violations. 
	 */
	String timestamp;
	
	/**
	 * Constructor for creating the notification class from a class containing data from line of a telemetry 
	 * archive file.
	 * 
	 * @param input The data from a single line in a telemetry archive file, in class form.
	 * @param s The severity ("RED HIGH", "RED LOW")
	 */
	public OutputDTO(InputDTO input, String s) {
		satelliteId=input.getSatelliteID();
		severity=s;
		component=input.getComponent();
		
		try {
			timestamp=Constants.OUTPUT_DATETIME_FORMAT.format(input.getTimeStampLong());
		} catch (IllegalArgumentException E) {
			System.out.println("Error formatting output timestamp: <"+E.getLocalizedMessage()+">");
			timestamp="ERROR";
		} catch (Exception E) {
			System.out.println("Unexpected exception when formatting a timestamp: <"+E.getLocalizedMessage()+">");
			timestamp="ERROR";
		} 
	}
	
	/**
	 * Intended for JUNIT testing only.
	 * Constructor for when all four components are known.
	 * 
	 * @param si Satellite ID
	 * @param sv Severity ("RED HIGH", "RED LOW")
	 * @param co Component ("BATT", "TSTAT")
	 * @param ts Timestamp (Format not checked -- assumes the correct format is sent as this is only called 
	 *           from a JUNIT test) 
	 */
	public OutputDTO(int si, String sv, String co, String ts) {
		satelliteId=si;
		severity=sv;
		component=co;
		timestamp=ts;
	}
	
	/**
	 * Intended for JUNIT testing only.
	 * Constructor for when data is su0pplied as a pipe-delimted string.
	 * 
	 * @param data Pipe-delimeted text string (Format not checked -- assumes the correct format is sent 
	 *  		   as this should only get called from a JUNIT test) 
	 */
	public OutputDTO(String data) {
		String[] pieces=data.split("\\|");
		satelliteId=Integer.parseInt(pieces[0]);
		severity=pieces[1];
		component=pieces[2];
		timestamp=pieces[3];
	}
}
