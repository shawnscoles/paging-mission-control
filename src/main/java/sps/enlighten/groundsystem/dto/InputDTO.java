package sps.enlighten.groundsystem.dto;

import java.text.ParseException;

import lombok.Getter;
import lombok.Setter;
import sps.enlighten.groundsystem.constants.Constants;

/**
 * Data contained on each line of the telemetry archive file.
 * 
 * @author Shawn Scoles
 *
 */
@Getter
@Setter
public class InputDTO {
	
	/**
	 * Telemetry timestamp in the YYYYMMDD HH:MM:SS.MMM format.
	 */
	String timeStamp;
	
	/**
	 * Since the sample output shows the satellite ID as an integer, it is assumed the input is the same. 
	 */
	int satelliteID;
	
	/**
	 * Red high limit for the "component" value.
	 */
	float redHighLimit;
	
	/**
	 * Yellow high limit for the "component" value.
	 */
	float yellowHighLimit;
	
	/**
	 * Yellow low limit for the "component" value.
	 */
	float yellowLowLimit;
	
	/**
	 * Red low limit for the "component" value.
	 */
	float redLowLimit;
	
	/**
	 * Actual value.
	 */
	float rawValue;
	
	/**
	 * Which component?  For the purpose of this programming challenge, only BATT and TSTAT are valid.
	 *    BATT: Battery Voltage
	 *    TSTAT: Thermostat Temperature
	 */
	String component;
	
	/**
	 * Assuming the sample data file for the task has correct formatting (YYYYMMDD HH:MM:SS.MMM), 
	 * this is the time stamp in milliseconds from 1970/01/01 00:00:00.000. 
	 */
	long timeStampLong;
	
	/**
	 * Takes a line of data from a telemetry file and fills in the class.
	 * Since the task specifies 
	 *    "You may assume that the input files are correctly formatted. 
	 *     Error handling for invalid input files may be omitted."
	 * No error checking is done on the string-to-number and time-to-number conversions.
	 * 
	 * @param data Line of data from a telemetry file.
	 */
	public InputDTO(String data) {
		String[] pieces=data.split("\\|");
		timeStamp=pieces[0].trim();
		satelliteID=Integer.parseInt(pieces[1].trim());
		redHighLimit=Float.parseFloat(pieces[2].trim());
		yellowHighLimit=Float.parseFloat(pieces[3].trim());
		yellowLowLimit=Float.parseFloat(pieces[4].trim());
		redLowLimit=Float.parseFloat(pieces[5].trim());
		rawValue=Float.parseFloat(pieces[6].trim());
		component=pieces[7].trim();
		
		try {
			timeStampLong=Constants.INPUT_DATETIME_FORMAT.parse(timeStamp).getTime();
		} catch (ParseException E) {
			// As per the task definition, it is assumed the input file is in the correct format and
			// this includes the date.  Therefore this exception does not need to be handled. 
		} 
	}
}
