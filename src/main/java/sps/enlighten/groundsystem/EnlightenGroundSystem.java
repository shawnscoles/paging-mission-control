package sps.enlighten.groundsystem;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * The Enlightened IT Consulting Ground System
 * 
 * Assumptions, based on the requirements:
 *   - 2 Satellites
 *   - 2 Components (Battery "BATT" and Thermostat "TSTAT")
 *   - Data in the telemetry file is already in chronological order as per the sample data example.
 *   - Only monitoring for violations of the Battery Red Low and Thermostat Red High limits.
 *   - Satellite IDs are numeric.  
 *   - Timestamps are already GMT+0 (ZULU) time.
 *   
 * Required libraries:
 *   lombok.jar - Contains class and member annotations to automatically create getter and setter 
 *                functions for all class members.
 *   gson.jar - The Google JSON processor -- allows for easy conversion between JSON strings and 
 *              Java classes. 
 * 
 * @author Shawn Scoles
 *
 */
public class EnlightenGroundSystem {

	/**
	 * Arguments:
	 *   args[1]=Telemetry File Name (required)
	 * 
	 * @param args Just the name of the telemetry file to process.
	 * 
	 */
	public static void main(String[] args) {
		
		// Add whitespace between the command and the output text.
		System.out.println("");
		
		// Check to see if a telemetry file was specified
		if (args.length<1) {
			System.out.println("Please specify a telemetry file to process.");
		} else {
			
			// If success, display the notifications.
			// If failure, an error message would have been displayed by the processTelemetryFile method.
			if (processTelemetryFile(args[0])) {
				System.out.println("Satellite Operator Notifications:");
				System.out.println("---------------------------------");
				System.out.println(TelemetryHandler.createResultJson());
			}
		}
	}
	
	/**
	 * Process the telemetry file.
	 * 
	 * @param fileName The name of the telemetry file to process.  
	 * 
	 * @return TRUE if succcessful, FALSE if not.
	 */
	public static boolean processTelemetryFile(String fileName) {
		
		// Assume success
		boolean retval=true;
		
		// Create a file object to check for file existence.
		File telemetrySourceFile=new File(fileName);
		
		// Does the file exist?
		if (telemetrySourceFile.exists()) {
			
			// The data from the telemetry file.
			String telemetryData=null;
			try {
				
				// Read the telemetry file.
				telemetryData=new String(Files.readAllBytes(Paths.get(fileName)));
				
				// Split input file into individual lines.  Lines could end in \n\r or \n, 
				// depending on the source. 
				String[] linesOfData=telemetryData.split("\\r\\n|\\n");
				
				// Process each line one at a time.
				for (String line:linesOfData) 
					if (line!=null && line.trim().length()>0)
						TelemetryHandler.processLine(line);
				
			// File is too big for memory
			} catch (OutOfMemoryError E) {     
				System.out.println("ERROR: Unable to load file.  File is too big to fit in available memory.");
				retval=false;
				
			// File cannot be read
			} catch (SecurityException E) {
				System.out.println("ERROR: Unable to load file.  File is read-protected and cannot be read by "+
						"the user running this script.");
				retval=false;
				
			// Any other IO Exception
			} catch (IOException E) {
				System.out.println("ERROR: Unable to load file.  File Input/Output Exception: <"+E.getLocalizedMessage()+">");
				retval=false;
				
		    // Catch unexpected exceptions.
			// Note that an InvalidPathException is one of the possible exceptions from Paths.get(), but
			//   since at this point the existence of the file "fileName" has been validated, an 
			//   InvalidPathException would never be thrown.
			} catch (Exception E) {
				System.out.println("ERROR: Unable to load file.  Unexpected Exception: <"+E.getLocalizedMessage()+">");
				E.printStackTrace();
				retval=false;
			}
			
		// If the file does not exist, display a message.  
		} else {
			System.out.println("ERROR: File does not exist: <"+fileName+">.");
			retval=false;
		}
		
		return retval;
	}
}
