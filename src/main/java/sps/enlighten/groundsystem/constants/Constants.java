package sps.enlighten.groundsystem.constants;

import java.text.SimpleDateFormat;

public class Constants {
	
	public static final String RED_HIGH="RED HIGH";
	public static final String RED_LOW="RED LOW";
	public static final String COMPONENT_BATTERY="BATT";
	public static final String COMPONENT_THERMOSTAT="TSTAT";
	public static final long FIVE_MINUTES_IN_MILLISECONDS=5*60*1000L;
	
	/**
	 * Defines the format of the timestamp.
	 * 
	 * Note the 'Z' hard-coded at the end of the string.  From the sample data, no timezone was specified,
	 *   so the output timestamp has to be assumed as GMT [ZULU].  
	 */
	public static final SimpleDateFormat OUTPUT_DATETIME_FORMAT=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	
	/**
	 * Defines the format of the timestamp. 
	 */
	public static final SimpleDateFormat INPUT_DATETIME_FORMAT=new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");

}
