package sps.enlighten.groundsystem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import sps.enlighten.groundsystem.constants.Constants;
import sps.enlighten.groundsystem.dto.InputDTO;
import sps.enlighten.groundsystem.dto.OutputDTO;

/**
 * Class to handle the processing of telemetry.
 * 
 * @author Shawn Scoles
 *
 */
public class TelemetryHandler {

	/**
	 * Used to keep track of individual limit violations. 
	 */
	static HashMap<String,LinkedList<InputDTO>> violations=new HashMap<>();
	
	/**
	 * Every notification to send to the operator for limit violations found
	 */
	static List<OutputDTO> operatorNotifications=new ArrayList<>(); 
	
	/**
	 * Contains keys for all messages to be sent to the operator.  Format: "satelliteId:componentName:timeStamp" 
	 * 
	 * TreeSet used instead of ArrayList to allow a quicker lookup of existing strings.
	 */
	static TreeSet<String> usedTimestamps=new TreeSet<>();
	
	/**
	 * Used by the JUnit tests to clear out all used memory before running any test.
	 * Since all the data in this class is static, it remains in memory between tests, so a method is 
	 *   needed to clear memory.
	 */
	public static void reset() {
		violations.clear();
		operatorNotifications.clear();
		usedTimestamps.clear();
	}
	 
	/**
	 * Processes a line of telemetry.
	 * 
	 * @param line A single line from the telemetry archive file.
	 */
	public static void processLine(String line) {
		
		// Create an InputDTO object from the line of text from the telemetry file.
		InputDTO inputData=new InputDTO(line);
		
		// Get the linked list for this satellite/component pair 
		String key=inputData.getSatelliteID()+":"+inputData.getComponent();
		LinkedList<InputDTO> current=violations.get(key);

		// Check to see if either of the two limit violations exist in this line.
		// As per the requirements, we are only checking thermostat red high and battery red low.
		if ((inputData.getRawValue()>=inputData.getRedHighLimit() && 
				inputData.getComponent().equalsIgnoreCase(Constants.COMPONENT_THERMOSTAT)) ||
			(inputData.getRawValue()<=inputData.getRedLowLimit() && 
				inputData.getComponent().equalsIgnoreCase(Constants.COMPONENT_BATTERY))){
			
			// Which limit violation was seen?
			String type=null;
			if (inputData.getRawValue()>=inputData.getRedHighLimit() && 
					inputData.getComponent().equalsIgnoreCase(Constants.COMPONENT_THERMOSTAT))
						type=Constants.RED_HIGH;
			else type=Constants.RED_LOW;
			
			// If no records exist for the satellite/component pair, create a new LinkedList object.
			if (current==null) current=new LinkedList<>();
			
			// Add the current record to the start of the linked list.
			current.add(inputData);
			
			// Check to see if three of these limit violations have occurred within the past 5 minutes.
			// The "current" LinkedList is updated if needed to remove
			checkIfNotifyNeeded(current, type);
			
			// Save the updated LinkedList.
			violations.put(key,current);
			
		// If either one of these variables have not been defined, there is no reason to check for 
		// unreported limit violations. 
		} else if (inputData!=null && current!=null) {
			
			// Although the current record was not a limit violation, that does not mean the past 
			// 5 minutes did not have three already queued.  
			flush(inputData, current);
		}
	}
	
	
	/**
	 * Checks the telemetry record for limit violations.  If found, add it to the violations queue and 
	 * potentially the operator message queue.
	 * 
	 * @param checkMe The telemetry record to check. 
	 */
	static void checkIfNotifyNeeded(LinkedList<InputDTO> checkMe, String severity) {

		// Remove any limit violations from the list that are more than five minutes older than the 
		// most recent limit violation.  Also, if the list size is less than 3, there is no point to 
		// continue checking for old violations as the requirement is notify if there are 3 violations 
		// within any 5 minute period.
		while (checkMe.size()>2 && checkMe.getLast().getTimeStampLong()-checkMe.getFirst().getTimeStampLong()>
			Constants.FIVE_MINUTES_IN_MILLISECONDS) 
				checkMe.removeFirst();

		// At this point, the "checkMe" queue has all limit violations within the 5 minutes prior to 
		//    the most recent limit violation.
		//
		// Requirement is notify if 3 violations within 5 minutes, so if there are less than 3, no 
		//    notifications are needed.
		//
		// NOTE: The following scenario will generate 2 messages:
		//    00:00:00 All Good
		//    00:01:00 All Good
		//    00:02:00 All Good
		//    00:03:00 Limit Violation
		//    00:04:00 Limit Violation
		//    00:05:00 Limit Violation
		//    00:06:00 Limit Violation
		//    00:07:00 All Good
		//    00:08:00 All Good
		//    00:09:00 All Good
		//
		// This behavior is intentional.  Operator notifications will be timestamped as:
		//    00:03:00     (for the 00:01:00 to 00:06:00 time range -- 00:03:00 is the first violation of 
		//					three or more within this time range/)
		//    [NO MESSAGE] (for the 00:02:00 to 00:07:00 time range -- No message because we do not want to 
		//                  duplicate timestamps)
		//    [NO MESSAGE] (for the 00:03:00 to 00:08:00 time range -- No message because we do not want to 
		//                  duplicate timestamps)
		//    00:04:00     (for the 00:04:00 to 00:09:00 time range)
		//
		if (checkMe.size()>2) {
			
			// Need a unique key to determine if an operator notification has already been created for this 
			// cluster of limit violations.
			String deduplicationKey=checkMe.getFirst().getSatelliteID()+":"+checkMe.getFirst().getComponent()+":"+
					checkMe.getFirst().getTimeStamp();

			// If the first violation within the time range has not been seen, add the notification to the output.
			if (!usedTimestamps.contains(deduplicationKey)) {
				OutputDTO newMessage=new OutputDTO(checkMe.getFirst(), severity);
				operatorNotifications.add(newMessage);
				usedTimestamps.add(deduplicationKey);
			}
		}
	}

	
	/**
	 * Flush any unreported limit violation messages.
	 * 
	 * @param current The current telemetry record -- used as a timestamp.
	 * @param checkMe The LinkedList the current record would have been inserted in if it was a limit violation.
	 */
	static void flush(InputDTO current, LinkedList<InputDTO> checkMe) {

		// Remove any limit violations from the list that are more than five minutes older than the 
		// most recent limit violation.  Need to verify there is at least one item in the linked list
		// before checking timestamp differences.
		while (checkMe.size()>1 && current.getTimeStampLong()-checkMe.getFirst().getTimeStampLong()>
			Constants.FIVE_MINUTES_IN_MILLISECONDS) 
				checkMe.removeFirst();
		
		// If the queue is still more than 2 items... 
		if (checkMe.size()>2) {
			
			// Need a unique key to determine if an operator notification has already been created for this 
			// cluster of limit violations.
			String deduplicationKey=checkMe.getFirst().getSatelliteID()+":"+checkMe.getFirst().getComponent()+":"+
					checkMe.getFirst().getTimeStamp();

			// If the first violation within the time range has not been seen, add the notification to the output.
			if (!usedTimestamps.contains(deduplicationKey)) {

				// Which limit violation was seen?
				String severity=null;
				if (checkMe.getFirst().getRawValue()>=checkMe.getFirst().getRedHighLimit() && 
						checkMe.getFirst().getComponent().equalsIgnoreCase(Constants.COMPONENT_THERMOSTAT))
					severity=Constants.RED_HIGH;
				else severity=Constants.RED_LOW;
				
				OutputDTO newMessage=new OutputDTO(checkMe.getFirst(), severity);
				operatorNotifications.add(newMessage);
				usedTimestamps.add(deduplicationKey);
			}
		}
	}
	
	/**
	 * Create the result JSON string.
	 * 
	 * @return A nicely formatted version of the violations list.
	 */
	public static String createResultJson() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(operatorNotifications);
	}
}
