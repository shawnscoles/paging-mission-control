From the root folder (where pom.xml is located) - compile the code and create 
    the .jar with the following command:

        mvn clean compile assembly:single


Run unit tests from the root folder:
        mvn test


Run the code from the root folder:
    java -cp target/enlighten-ground-system-1.0-jar-with-dependencies.jar 
         sps.enlighten.groundsystem.EnlightenGroundSystem <telemetryFileName>

    Where:
        <telemetryFileName> = The name of the ASCII telemetry file.
